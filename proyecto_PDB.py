#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dialogos import Dialogos
from chooser_files import Seleccionador


class Principal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./proyecto_PDB.ui")

        self.window = self.builder.get_object("Main_ventana")
        self.window.set_title("Proyecto PDB")
        self.window.maximize()
        self.window.connect("destroy", Gtk.main_quit)
        
        
        # About
        Acerca_de = self.builder.get_object("Acerca_de")
        Acerca_de.connect("activate", self.boton_acerca_de)        
        
        # FileChooser para construccion de imagenes.
        chooser_de_files = self.builder.get_object("Abrir")
        chooser_de_files.connect("activate", self.boton_file_chooser)
        
        # Imagen
        self.imagen = self.builder.get_object("Image")
        
        self.window.show_all()
        
    
    def boton_acerca_de(self, btn=None):
        dlg = Dialogos(object_name="AcercaDe")
        dlg.dialogo.run()
        dlg.dialogo.destroy()
        
    def boton_file_chooser(self, btn=None):
        dlg = Seleccionador(object_name="Chooser")
        dlg.dialogo.run()
        
        
        
        #Extension_archivo = self.dlg().generar_imagen()        # Este fragmento es el que mostraria la imagen seleccionada
        #print(Extension_archivo)                               # en la ventana principal si entendiera como recibir el return 
                                                                # de la funcion "generar_imagen" en la clase "Seleccionador"
        #if (extension_archivo == ".png"):
            
            
            #self.imagen.set_from_file("nombre_archivo.png")    # Esta linea funciona con un archivo.png cualquiera.
        
        dlg.dialogo.destroy()

        


if __name__ == "__main__":
    Principal()
    Gtk.main()
