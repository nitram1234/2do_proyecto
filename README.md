Proyecto_PDB
Nombres: Martin Vergara, Lucas Pavez.
Asignatura: Programación avanzada.
Profesor: Fabio Durán.

Este programa ha sido realizado bajo los dogmas del lenguaje de programación Python3, además se han utilizado interfaces graficas obtenidas del programa Glade. Para realizar la parte de la visualización fue necesario contar con Pymol.
Descripción:
Al ejecutar el programa la primera ventana da la opción de Archivo y otro de ayuda. Si selecciona ayuda podrá ver el “acerca de”, y si selecciona archivo muestra un FileChooser el cual le permitirá abrir un archivo PDB que el código convierte en distintos archivos: ATOM, HETATM,OTHERS, etc.
 
Pre-requisitos:
Tener instalado: Python3, Pymol y Biopanda.

Este proyecto fue realizado con la finalidad de aprender a manejar la programación orientada a objetos y se puede aplicar a fines bioinformáticas. 

